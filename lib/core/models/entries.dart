class Entries {
  String title;
  String description;

  Entries({
    this.title,
    this.description,
  });
}

List<Entries> entries = [
  Entries(
      title: 'Git init',
      description:
          'El comando git init crea un nuevo repositorio de Git. Puede utilizarse para convertir un proyecto existente y sin versión en un repositorio de Git o inicializar un nuevo repositorio vacío.'),
  Entries(
      title: 'Git clone',
      description:
          'git clone se utiliza principalmente para apuntar a un repositorio existente y clonar o copiar dicho repositorio en un nuevo directorio, en otra ubicación.'),
  Entries(
      title: 'Git clean',
      description:
          'Git clean es un comando para "deshacer". Git clean se puede considerar como complementario a otros comandos, por ejemplo, git reset y git checkout. Mientras que otros comandos actúan en archivos añadidos previamente al índice de seguimiento de Git, el comando git clean actúa en archivos sin seguimiento.'),
];
