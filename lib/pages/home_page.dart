import 'package:flutter/material.dart';
import 'package:pruebas_gitlab_pages/core/models/entries.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Comandos',
          style: TextStyle(color: Colors.blue),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
        color: Colors.white30,
        child: ListView(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Text(
                      entries[0].title,
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(height: 10),
                    Text(
                      entries[0].description,
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.grey, fontSize: 22),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Text(
                      entries[1].title,
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(height: 10),
                    Text(
                      entries[2].description,
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.grey, fontSize: 22),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Text(
                      entries[2].title,
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(height: 10),
                    Text(
                      entries[2].description,
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.grey, fontSize: 22),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
